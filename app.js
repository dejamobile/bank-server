var express = require('express');
var path = require('path');
var cors = require('cors')
var logger = require('morgan');

var userRouter = require('./routes/user');

var app = express();

app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const authMiddleware = (req, res, next) => {
  const token = req.headers["authorization"];
  if(token != "123456"){
    res.status(400).send("Invalid token.");
  }
  else {
    next();
  }
}
app.use(authMiddleware);
app.use('/user', userRouter);

module.exports = app;
