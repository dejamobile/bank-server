var express = require('express');
var router = express.Router();
var luhnify = require('luhnify');

router.get('/info', function(req, res, next) {
  res.send({
    'name':'alexandre',
    'cardNumber': luhnify( '4### #### #### ####' )
  });
});

module.exports = router;
